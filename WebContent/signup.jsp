<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>
 <base href="<%=basePath%>">   
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>sign-up</title>        
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<%=basePath%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=basePath%>/css/animate.css">
<link rel="stylesheet" href="<%=basePath%>/css/style.css">
<script type="text/javascript">

</script>
</head>
<body>
<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<!-- Start Sign In Form -->
					<form action="insertUser" class="fh5co-form animate-box" data-animate-effect="fadeIn" method="post" name="upup">
						<h2>Sign Up</h2>
						<div id="message">${error}<span id="showname" style="display:inline;"></span></div>
						<div class="form-group">
							<div class="alert alert-success" role="alert">Your info has been saved.</div>
						</div>
						<div class="form-group">
							<label for="name" class="sr-only">Name</label>
							<input type="text" class="form-control" id="name" placeholder="Name" autocomplete="off" name="name" onblur="show()">
						</div>
						
						<div class="form-group">
							<label for="password" class="sr-only">Password</label>
							<input type="password" class="form-control" id="psw" placeholder="Password" autocomplete="off" name="psw" onblur="show()">
						</div>
						<div class="form-group">
							<label for="utele" class="sr-only">Tele</label>
							<input type="text" class="form-control" id="utele" placeholder="utele" autocomplete="off" name="utele" onblur="show()">
						</div>
						<div class="form-group">
							<label for="email" class="sr-only">Email</label>
							<input type="email" class="form-control" id="uemail" placeholder="Email" autocomplete="off" name="uemail">
						</div>
						<div class="form-group">
							<p>Already registered? <a href="login.html">Sign In</a></p>
						</div>
						<div class="form-group">
							<input type="submit" value="Sign Up" class="btn btn-primary" onclick="document.upup.submit();">
						</div>
					</form>
					<!-- END Sign In Form -->

				</div>
			</div>
</body>
</html>
