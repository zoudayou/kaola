<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.pojo.OUser"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<%
		OUser user=(OUser)session.getAttribute("user");
%>

<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学生登录界面</title>

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300'
	rel='stylesheet' type='text/css'>


<!-- Bootstrap Core CSS -->
<link
	href="<%=basePath%>bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=basePath%>bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<%=basePath%>dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=basePath%>bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<script type="text/javascript" src="<%=basePath%>js/jquery-3.1.1.js"></script>

<script type="text/javascript" language="javascript">
	function iFrameHeight() {
		var ifm = document.getElementById("iframecon");
		var subWeb = document.frames ? document.frames["iframepage"].document
				: ifm.contentDocument;
		if (ifm != null && subWeb != null) {
			ifm.height = subWeb.body.scrollHeight;
		}
	}
	window.setInterval("iFrameHeight()", 200);
</script>

</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">菜单</span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="teacher.html">芯智考试管理系统</a>
		</div>
		<!-- /.navbar-header -->

		<ul class="nav navbar-top-links navbar-right">
			<!-- /.dropdown -->
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i>
					<i class="fa fa-caret-down"></i>
			</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="#"><i class="fa fa-user fa-fw"></i><%=user.getUname() %>
</a></li>
					<li><a href="#"><i class="fa fa-gear fa-fw"></i>设置</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="fa fa-sign-out fa-fw"></i>登出</a></li>
				</ul> <!-- /.dropdown-user --></li>
			<!-- /.dropdown -->
		</ul>
		<!-- /.navbar-top-links -->

		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				<ul class="nav" id="side-menu">
					<li><a id="test-paper-management"
						url="stu\stu-test.jsp"
						href="javascript:void(0)"><i class="fa fa-edit fa-fw"></i>考试系统</a>
					</li>
					<li><a href="javascript:void(0)"><i
							class="fa fa-table fa-fw"></i>学习系统<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a id="test-base-management-add" url=""
								href="javascript:void(0)">题库练习</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a id="personal-information"
						url="personal-information.html" href="javascript:void(0)"><i
							class="fa fa-wrench fa-fw"></i>个人信息管理</a> <!-- /.nav-second-level -->
					</li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>
		<!-- /.navbar-static-side --> </nav>

		<!-- Page Content -->
		<div id="page-wrapper">

			<iframe src="" marginheight="0"
				marginwidth="0" frameborder="0" scrolling="no" width="100%"
				height="100%" id="iframecon" name="iframepage"
				onLoad="iFrameHeight()"></iframe>

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<!--左边菜单控制切换右侧内容js-->
	<script>
		$(document).ready(function(e) {
			$("#test-paper-management").click(function() {
				var url = $(this).attr("url");
				$("#iframecon").attr("src", url);
			});
			$("#test-base-management-add").click(function() {
				var url = $(this).attr("url");
				$("#iframecon").attr("src", url);
			})
			$("#test-base-management-examine").click(function() {
				var url = $(this).attr("url");
				$("#iframecon").attr("src", url);
			})
			$("#personal-information").click(function() {
				var url = $(this).attr("url");
				$("#iframecon").attr("src", url);
			})
		});
	</script>
	<!-- jQuery -->
<script src="<%=basePath%>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<%=basePath%>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<%=basePath%>bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<%=basePath%>dist/js/sb-admin-2.js"></script>
</body>

</html>
