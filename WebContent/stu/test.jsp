<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>
 <base href="<%=basePath%>">   
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>test</title>        
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="0">    
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="This is my page">
</head>
<body>
<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<!-- Start Sign In Form -->
			<form action="insertTest" method="post" name="myform"
				class="fh5co-form animate-box" data-animate-effect="fadeIn">
				<h2>Test</h2>
				<div id="message">${error}<span id="showname"
						style="display: inline;"></span>
				</div>
				<div class="form-group">
					<label for="testname" class="sr-only">Tsername</label> <input
						type="text" class="form-control" id="tname" name="testname"
						onblur="show()" placeholder="Testname" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="subject" class="sr-only">Subject</label> <input
						type="text" class="form-control" id="pwd" name="subject"
						onblur="show()" placeholder="Subject" autocomplete="off">
				</div>

				<div class="form-group">
					<input type="button" value="Sign In" class="btn btn-primary"
						onclick="document.myform.submit();">
				</div>
			</form>
			<!-- END Sign In Form -->

		</div>
	</div>
</body>
</html>
