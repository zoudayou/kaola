<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>

<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<%=basePath%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=basePath%>/css/animate.css">
<link rel="stylesheet" href="<%=basePath%>/css/style.css">
<script type="text/javascript">
function show(){ 
        if(document.getElementById("name").value==""){
            document.getElementById("showname").innerHTML="用户名不能为空！";
            document.getElementById("name").focus();
            return false;
        }else if(document.getElementById("pwd").value==""){
            document.getElementById("showname").innerHTML="密码不能为空！";
            document.getElementById("pwd").focus();
            return false;
        }else{
            return true;
        }
</script>
</head>
<body>
	<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">


			<!-- Start Sign In Form -->
			<form action="adminis.html" method="post" name="myform" class="fh5co-form animate-box"
				data-animate-effect="fadeIn">
				<h2>Sign In</h2>
				<div id="message">${error}<span id="showname" style="display:inline;"></span></div>
				<div class="form-group">
					<label for="username" class="sr-only">Username</label> <input
						type="text" class="form-control" id="name"
						name="username" onblur="show()"
						value="<%=(String)session.getAttribute("username") %>"
						placeholder="Username" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="password" class="sr-only">Password</label> <input
						type="password" class="form-control" id="pwd"
						name="password" onblur="show()"
						value="<%=(String)session.getAttribute("password") %>"
						placeholder="Password" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="remember"><input type="checkbox" id="remember">
						Remember Me</label>
				</div>
				<div class="form-group">
					<p>
						Not registered? <a href="signup.jsp">Sign Up</a>
					</p>
				</div>
				<div class="form-group">
					<input type="button" value="Sign In" class="btn btn-primary" onclick="document.myform.submit();">
				</div>
			</form>
			<!-- END Sign In Form -->

		</div>
	</div>
</body>
</html>
