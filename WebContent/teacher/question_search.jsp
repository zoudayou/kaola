<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.List,com.pojo.OKnowledge,com.pojo.OQuestionbank"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>

<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>题目查询</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!-- Bootstrap Core CSS -->
<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="./bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="./dist/css/sb-admin-2.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="./bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!--[if lt IE 9]><!-->
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<!--<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!--<![endif]-->

<!-- jQuery -->
<script src="./bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js"></script>

<!-- <script charset="utf-8" src="kindeditor/kindeditor-all.js"></script>

<script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>

<script charset="utf-8" src="kindeditor/plugins/code/prettify.js"></script>
<script type="text/javascript">
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			cssPath : "kindeditor/plugins/code/prettify.css",
			uploadJson : 'kindeditor/jsp/upload_json.jsp',
			fileManagerJson : 'kindeditor/jsp/file_manager_json.jsp',
			allowFileManager : true,
			afterCreate : function() {
				this.loadPlugin('autoheight');
			},
			items : [ 'fullscreen', 'source', 'table', 'insertfile', 'link',
					'image', 'emoticons', '|', 'forecolor', 'hilitecolor',
					'bold', 'underline', 'removeformat', '|', 'justifyleft',
					'justifycenter', 'justifyright', 'preview' ]
		});

	});
	/*window.setInterval("KindEditor.ready()", 200);*/
</script> -->
<script type="text/javascript">

	function main() {

		alert("---");
		var knowledgepoint = $("#select1").find("option:selected").text();
		var questiontype = $("#select2").find("option:selected").text();
		var question = document.getElementById("keyvalue").value;	
		window.location.href = "searchAllques.html?knowledgepoint="+ knowledgepoint + "&questiontype=" + questiontype+ "&question=" + question;

	}
</script>

</head>

<%
	List list1 = (List) request.getAttribute("list1");
	List list2 = (List) request.getAttribute("list2");
	String knowledgepoint = (String) request.getAttribute("knowledgepoint");
	String questiontype = (String) request.getAttribute("questiontype");
	String question = (String) request.getAttribute("question");
	
%>

<%
						for (Object o2 : list2) {
							OQuestionbank oqb2 = (OQuestionbank) o2;
							
					%>
					<%=oqb2.getStatement1()%><%} %>

<body style="background: #FFF;">

	<script type="text/javascript">
		var cpro_id = "u2451801";
		(window["cproStyleApi"] = window["cproStyleApi"] || {})[cpro_id] = {
			at : "3",
			rsi0 : "750",
			rsi1 : "90",
			pat : "6",
			tn : "baiduCustNativeAD",
			rss1 : "#FFFFFF",
			conBW : "1",
			adp : "1",
			ptt : "0",
			titFF : "%E5%BE%AE%E8%BD%AF%E9%9B%85%E9%BB%91",
			titFS : "14",
			rss2 : "#000000",
			titSU : "0",
			ptbg : "90",
			piw : "0",
			pih : "0",
			ptp : "0"
		}
	</script>
	<div class="row">
		<center>
			<h2 class="page-header">题库查询</h2>
		</center>
		<div class="panel-body">
			<div class="col-md-4 column">
				<label><h4>按知识点查找：</h4></label> <select id="select1" name="select1">
					<option selected="selected">请选择</option>
					<%
						for (Object obj : list1) {
							OKnowledge ob = (OKnowledge) obj;
							String selected = "";
							if (knowledgepoint.equals(ob.getKnowledgepoint())) {
								selected = "selected='selected'";
							}
					%>
					<option <%=selected%>><%=ob.getKnowledgepoint()%></option>
					<%
						}
					%>


				</select>
			</div>
			<div class="col-md-4 column">
				<label><h4>按题型查找：</h4></label> <select id="select2" name="select2">
					<option value="0" <%=(questiontype.equals("请选择") ? "selected" : "")%>>请选择</option>
					<option value="1" <%=(questiontype.equals("选择题") ? "selected" : "")%>>选择题</option>
					<option value="2" <%=(questiontype.equals("判断题") ? "selected" : "")%>>判断题</option>
					<option value="3" <%=(questiontype.equals("填空题") ? "selected" : "")%>>填空题</option>
					<option value="4" <%=(questiontype.equals("简答题") ? "selected" : "")%>>简答题</option>
				</select>
			</div>
			<div class="col-md-4 column">
				<label><h4>按关键字查找：</h4></label> <input type="text" name=""
					id="keyvalue" value="" />
			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-12 column">
				<input class="btn btn-primary" type="button" name="" id=""
					value="查询" style="float: right" onclick="main()"><a
					href=""></a>
			</div>
		</div>
		<div class="panel-body" style="background-color: #D8D8D8;">

			<div class="col-md-4 column">
			<%					
					if (knowledgepoint.equals("")) {
						knowledgepoint = "java,html,css...";
						}
 					%> 

				<label><h3>知识点:</h3></label> <label><input type="text"
					name="" id="knowledge" value="<%=knowledgepoint%>"
					style="background-color: #D8D8D8;"> 
					
 					 </input></label>

			</div>
			<div class="col-md-8 column" >
				<form role="form" >
					<%
						for (Object o1 : list2) {
							OQuestionbank oqb1 = (OQuestionbank) o1;
					%>
					<div class="form-group" >
						<div class="col-md-12">
							<p>
								<label id="questionId"><%=oqb1.getQid()%></label>
								<%=oqb1.getQuestion()%>
							</p>
						</div>
						</br>
						<div class="col-md-4">
							题型:<label id="tixing"><%=oqb1.getQuestiontype()%></label>
						</div>
						<div class="col-md-4">
							分值:<label id="fenzhi"><%=oqb1.getQvalue()%></label>
						</div>
						<div class="col-md-4">
						
					
					<% String a="冻结";
					String b="不可见";
					
					if(oqb1.getStatement1()==1){
						a="恢复";
					}
					if(oqb1.getStatement2()==1){
						b="可见";
					}
					%>
						
							<input type="button" name="" id="btn1" value="<%=a %>"  onclick="fun1()"/>
							<input type="button" name="" id="btn2" value="<%=b %>"  onclick="fun2()"/>
							<input type="button" name="" id="btn3" value="修改"  />

						</div>
						<!-- <br />
						<textarea id="content" name="content" class="form-control"
							rows="3"></textarea> -->
					</div>
					<hr style="border:1px #b0b0b0 dotted;">
					 <%
						}
					%> 
				</form>
			</div>
		</div>
	</div>
	<script>
	  function fun1(){
		 var s= $("#btn1").val();
		 var s1=document.getElementById('btn1').value;
		 alert("s="+s+",s1="+s1);
		/*  var i1=$("#questionId").html().trim();
		 var i2=document.getElementById('questionId').innerText.trim();
			 
		 alert("s1="+s1+",questionId="+i2); */
		 var s={};
		 s.statement1=1;
		/*   if(s1.equals("冻结")){
			 s.statement1=1;
		 }
		 else{
			 s.statement1=0;
		 }  */
		 alert(JSON.stringify(s));
		 alert("s.statement1="+s.statement1);
		 $.ajax({
				type : 'post',
				url : 'updateStatement1.html',
				data : s,
				success : function(data) {
					console.log(data)
				}
			});
		 /* if(s1=="冻结"){
			 statement1=1;
			 document.getElementById('btn1').value="恢复";
		 }
		 else{
			 statement1=0;
			 document.getElementById('btn1').value="冻结";
		 } */
		
	}  
	</script>
	<!-- 	<script type="text/javascript">
    var selectedIndex = getCookie("select1");
    if(selectedIndex != null) {
        document.getElementById("select1").selectedIndex = selectedIndex;
    }
</script>
<script type="text/javascript">
    var selectedIndex = getCookie("select2");
    if(selectedIndex != null) {
        document.getElementById("select2").selectedIndex = selectedIndex;
    }
</script> -->

</body>
</html>
