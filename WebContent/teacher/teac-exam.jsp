<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>
 <base href="<%=basePath%>">   
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>考生试卷管理</title>        
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="0">    
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="This is my page">
  <!-- Bootstrap Core CSS -->
		<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

		<!-- MetisMenu CSS -->
		<link href="./bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="./dist/css/sb-admin-2.css" rel="stylesheet">

		<!-- Custom Fonts -->
		<link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
		<!--iframe自适应内容高度js-->
		<script type="text/javascript" language="javascript">
			function iFrameHeight2() {
				var ifm = document.getElementById("iframecon2");
				var subWeb = document.frames ? document.frames["iframepage2"].document : ifm.contentDocument;
				if(ifm != null && subWeb != null) {
					ifm.height = subWeb.body.scrollHeight;
				}
			}
		</script>
</head>

<body style="background:#FFF;">

		<script type="text/javascript">
			var cpro_id = "u2451801";
			(window["cproStyleApi"] = window["cproStyleApi"] || {})[cpro_id] = {
				at: "3",
				rsi0: "750",
				rsi1: "90",
				pat: "6",
				tn: "baiduCustNativeAD",
				rss1: "#FFFFFF",
				conBW: "1",
				adp: "1",
				ptt: "0",
				titFF: "%E5%BE%AE%E8%BD%AF%E9%9B%85%E9%BB%91",
				titFS: "14",
				rss2: "#000000",
				titSU: "0",
				ptbg: "90",
				piw: "0",
				pih: "0",
				ptp: "0"
			}
		</script>
		<!-- /.row -->

		<div class="row">
			<div class="col-lg-3 col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12 text-center">
								<div class="huge">待考试完成</div>
								<!--<div>New Tasks!</div>-->
							</div>
						</div>
					</div>
					<a url="teacher-(test-paper-management-to-be-completed).html" href="javascript:void(0)" id="to-be-completed">
						<div class="panel-footer">
							<span class="pull-left">查看</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12 text-center">
								<div class="huge">待批阅</div>
								<!--<div>New Tasks!</div>-->
							</div>
						</div>
					</div>
					<a url="./pages/flot.html" href="javascript:void(0)" id="to-read">
						<div class="panel-footer">
							<span class="pull-left">查看</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12 text-center">
								<div class="huge">已完成批阅</div>
								<!--<div>New Orders!</div>-->
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">查看</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3">
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12 text-center">
								<div class="huge">历史试卷</div>
								<!--<div>Support Tickets!</div>-->
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">查看</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<div >

			<iframe src="" marginheight="0" marginwidth="0" frameborder="0" scrolling="no" width="100%" height="100%" id="iframecon2" name="iframepage2" onLoad="iFrameHeight2()"></iframe>

		</div>

		<!-- jQuery -->
		<script src="./bower_components/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="./dist/js/sb-admin-2.js"></script>
		<!--左边菜单控制切换右侧内容js-->
		<script>
			$(document).ready(function(e) {
				$("#to-be-completed").click(function() {
					var url = $(this).attr("url");
					$("#iframecon2").attr("src", url);
				});
				$("#to-read").click(function() {
					var url = $(this).attr("url");
					$("#iframecon2").attr("src", url);
				});

			});
		</script>

	</body>
</html>
