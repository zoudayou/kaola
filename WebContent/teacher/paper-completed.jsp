<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>待考试完成</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!-- Bootstrap Core CSS -->
<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="./bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="./dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="./bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- jQuery -->
<script src="./bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js"></script>
</head>
<body style="background: #FFF;">
	<div class="row">
		<div class="col-lg-12">
			<div class="jumbotron">
				<center>
					<h2>新建考试</h2>
					<p>如您需要新建一个考试，请点击下方新建按钮</p>
					<p>
						<a class="btn btn-primary btn-lg" role="button">新建</a>
					</p>
				</center>
			</div>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<center>
							<h2>试卷详情</h2></center>
					</div>
					<div class="panel-body">
						<div class="col-md-12">
							<div id="main" class="panel panel-default">
							</div>
						</div>

					</div>
				</div>
			</div>
	
	<script type="text/javascript">
		var cpro_id = "u2451801";
		(window["cproStyleApi"] = window["cproStyleApi"] || {})[cpro_id] = {
			at : "3",
			rsi0 : "750",
			rsi1 : "90",
			pat : "6",
			tn : "baiduCustNativeAD",
			rss1 : "#FFFFFF",
			conBW : "1",
			adp : "1",
			ptt : "0",
			titFF : "%E5%BE%AE%E8%BD%AF%E9%9B%85%E9%BB%91",
			titFS : "14",
			rss2 : "#000000",
			titSU : "0",
			ptbg : "90",
			piw : "0",
			pih : "0",
			ptp : "0"
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
    		$("#k").ready(function(){
    			
        		$.ajax({
            		url:"SelectTestinfo",
            		data:{},        		
            		type:"POST",
             		contentType: "application/json", 
            		dataType:"JSON",
            		success:function(msg){
            			
            			var lis=msg;
            			var html=''
            			for(var i in lis){

            				var a=lis[i].testid
                    		var b=lis[i].testname
                    		var c=lis[i].testsub
                    		var d=lis[i].testtime
                    		var e=lis[i].endtime
                    		var f=lis[i].cname
                    		var g=lis[i].indstudent
                    		var h=lis[i].attendnum
                    		var j=lis[i].allnum 
            
            				html+='<div class="col-md-12"><div class="panel panel-default">'+
                    		'<div class="panel-heading"><center><h3>'+a+","+b+'</h3></center></div>'+
							'<div class="panel-body"><div class="well well-lg"><div class="row">'+
								'<div class="col-md-2"><center><h5>知识点</h5>'+c+'</center></div>'+
								'<div class="col-md-2"><center><h5>开始时间</h5>'+d+'</center></div>'+
								'<div class="col-md-2"><center><h5>结束时间</h5>'+e+'</center></div>'+
								'<div class="col-md-2"><center><h5>参考人员</h5>'+f+","+g+'</center></div>'+
								'<div class="col-md-2"><center><h5>完成情况</h5><a style="color: red;">'+h+'</a>/'+j+'</center></div>'+
								'<div class="col-md-2"><center><h5>操作</h5><button type="button" class="btn btn-outline btn-primary btn-xs disabled">不可删除</button></center></div>'+								
						'</div></div></div></div></div>'
						
            			}            			                                		
                		$("#main").append(html)
            		},
            		error:function(e){
            			console.log(e);
            		}
        		})
    		});
		});
		
	</script>
	
	
	
</body>
</html>

