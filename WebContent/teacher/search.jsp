<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>test</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript" src="<%=path%>/js/jquery-3.1.1.js"></script>

</head>
<body>
	<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<!-- Start Sign In Form -->
			<form action="insertQuestion" method="post" name="myform"
				class="fh5co-form animate-box" data-animate-effect="fadeIn">
				<h2>Test</h2>
				<div id="message">${error}<span id="showname"
						style="display: inline;"></span>
				</div>
				<div class="form-group">
					<label for="testname" class="sr-only">题型</label> <input type="text"
						class="form-control" id="knowlegepoint" name="questiontype"
						onblur="show()" placeholder="questiontype" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="subject" class="sr-only">知识点</label> <input type="text"
						class="form-control" id="knowledgepoint" name="knowledgepoint"
						onblur="show()" placeholder="knowledgepoint" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="subject" class="sr-only">题目</label> <input type="text"
						class="form-control" id="question" name="question" onblur="show()"
						placeholder="question" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="subject" class="sr-only">答案</label> <input type="text"
						class="form-control" id="modanswer" name="modanswer"
						onblur="show()" placeholder="modanswer" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="subject" class="sr-only">分值</label> <input type="text"
						class="form-control" id="qvalue" name="qvalue" onblur="show()"
						placeholder="qvalue" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="subject" class="sr-only">备注</label> <input type="text"
						class="form-control" id="assess" name="assess" onblur="show()"
						placeholder="assess" autocomplete="off">
				</div>

				<div class="form-group">
					<input type="button" value="插入" class="btn btn-primary"
						onclick="document.myform.submit();"><!--  <input type="button"
						value="点击获取结果" id="btn" onclick="k"> <br> result
					: <span id="x"></span> -->
				</div>
			</form>
<script type="text/javascript">
$(document).ready(function(){
    $("#k").click(function(){
    	/* alert('dd') */
    	
        $.ajax({
            url:"findAll",
            data:{},
            type:"POST",
            contentType: "application/json", 
            dataType:"JSON",
            success:function(msg){
            	alert('dd')
            	var lis= JSON.parse(msg);
                for(var i=0;i<lis.length;i++){
                    var a=lis[i].qid
                    var b=lis[i].knowledgepoint
                    var c=lis[i].question
                    var d=lis[i].qoption
                    var e=lis[i].modanswer
                    var f=lis[i].qvalue
                    var g=lis[i].assess
                    var h=lis[i].statement1
                    var i=lis[i].statement2
                    var tr="<tr>"
                        tr+="<td>"+a+"</td>"
                        tr+="<td>"+b+"</td>"
                        tr+="<td>"+c+"</td>"
                        tr+="<td>"+d+"</td>"
                        tr+="<td>"+e+"</td>"
                        tr+="<td>"+f+"</td>"
                        tr+="<td>"+g+"</td>"
                        tr+="<td>"+h+"</td>"
                        tr+="<td>"+i+"</td>"
                        tr+="</tr>"
                        $("#x").append(tr)
                }    
            },
            error:function(e){
            	console.log(e);
            }
        })
    });
});
</script>

		</div>
	</div>
</body>
</html>
