<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List,com.pojo.OKnowledge"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>题库新增</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!-- Bootstrap Core CSS -->
<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="./bower_components/metisMenu/dist/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="./dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="./bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!--[if lt IE 9]><!-->
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<!--<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!--<![endif]-->
<!-- jQuery -->
<script src="./bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js"></script>

<script charset="utf-8" src="kindeditor/kindeditor-all.js"></script>
<script charset="utf-8" src="kindeditor/lang/zh_CN.js"></script>
<link rel="stylesheet" href="kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="kindeditor/plugins/code/prettify.js"></script>
<script type="text/javascript">
	var editor1, editor2, editor3;
	KindEditor.ready(function(K) {
		editor1 = K.create('textarea[name="content1"]', {
			cssPath : "kindeditor/plugins/code/prettify.css",
			uploadJson : 'kindeditor/jsp/upload_json.jsp',
			fileManagerJson : 'kindeditor/jsp/file_manager_json.jsp',
			allowFileManager : true,
			afterCreate : function() {
				this.loadPlugin('autoheight');
			},
			items : [ 'fullscreen', 'source', 'table', 'insertfile', 'link',
					'image', 'emoticons', '|', 'forecolor', 'hilitecolor',
					'bold', 'underline', 'removeformat', '|', 'justifyleft',
					'justifycenter', 'justifyright', 'preview' ]
		});
		editor2 = K.create('textarea[name="content2"]', {
			cssPath : "kindeditor/plugins/code/prettify.css",
			uploadJson : 'kindeditor/jsp/upload_json.jsp',
			fileManagerJson : 'kindeditor/jsp/file_manager_json.jsp',
			allowFileManager : true,
			newlineTag : "br",
			resizeType : 2,
			allowPreviewEmoticons : true,
			allowImageUpload : true,
			minHeight : "100px",
			autoHeightMode : false,
			afterCreate : function() {
				this.loadPlugin('autoheight');
			},
			items : [ 'fullscreen', 'source', 'table', 'insertfile', 'link',
					'image', 'emoticons', '|', 'forecolor', 'hilitecolor',
					'bold', 'underline', 'removeformat', '|', 'justifyleft',
					'justifycenter', 'justifyright', 'preview' ]
		});
	     
		 editor3 = K.create('textarea[name="content3"]', {
			cssPath : "kindeditor/plugins/code/prettify.css",
			uploadJson : 'kindeditor/jsp/upload_json.jsp',
			fileManagerJson : 'kindeditor/jsp/file_manager_json.jsp',
			allowFileManager : true,
			afterCreate : function() {
				this.loadPlugin('autoheight');
			},
			items : [ 'fullscreen', 'source', 'table', 'insertfile', 'link',
					'image', 'emoticons', '|', 'forecolor', 'hilitecolor',
					'bold', 'underline', 'removeformat', '|', 'justifyleft',
					'justifycenter', 'justifyright', 'preview' ]
		}); 
		$("#d4").css("display","none");
		 
	});
	//测试kineditor的上传和下载
	/* function submitForm()
	{
		$("#contentView").html(editor1.html());
		
	} */
</script>

<script>
	var info={
		modanswer:""
	}
	function main() {
		alert("---"); 	 
		info.questiontype = $("#select").find("option:selected").text();
		info.knowledgepoint = $("#knowlege").find("option:selected").text();
		info.statement2=document.getElementById("cb").value;
		info.question = editor1.html();
		info.qoption = $("#answer").val();
		fun5();
		info.qvalue = $("#qvalue").val();
		info.assess = editor2.html();
		alert(JSON.stringify(info));
		$.ajax({
			type : 'post',
			url : 'insertQuestion.html',
			data : info,
			success : function(data) {
				console.log(data)
			}
		});
		window.location.reload();
	}
	function fun5(){
		if(info.questiontype=="选择题"){
			var a=fun3();
			var b=a.join(",");
			info.modanswer=b;		
		}
		if(info.questiontype=="判断题"){			
			info.modanswer=document.getElementById("panduan").value;
		}
		if(info.questiontype=="填空题"){
			info.modanswer=$("#tiankong").val();
		}
		 if(info.questiontype=="简答题"){
			info.modanswer=editor3.html()
		} 
		
	};
</script>
</head>
<%
	List list = (List) request.getAttribute("list");
%>
<body style="background: #FFF;">

	<script type="text/javascript">
		var cpro_id = "u2451801";
		(window["cproStyleApi"] = window["cproStyleApi"] || {})[cpro_id] = {
			at : "3",
			rsi0 : "750",
			rsi1 : "90",
			pat : "6",
			tn : "baiduCustNativeAD",
			rss1 : "#FFFFFF",
			conBW : "1",
			adp : "1",
			ptt : "0",
			titFF : "%E5%BE%AE%E8%BD%AF%E9%9B%85%E9%BB%91",
			titFS : "14",
			rss2 : "#000000",
			titSU : "0",
			ptbg : "90",
			piw : "0",
			pih : "0",
			ptp : "0"
		}
	</script>
	<div class="row">
		<!-- <form action="insertQuestion.html" method="post" name="myform"> -->
		<center>
			<h2 class="page-header">题库录入</h2>
		</center>
		<div class="panel-body">
			<div class="col-md-2 column">
				<label><h3>题型</h3></label>
			</div>
			<div class="col-md-10 column">

				<div class="form-group">
					<br /> <select id="select" name="select">
						<option value="0">请选择……</option>
						<option value="1">选择题</option>
						<option value="2">判断题</option>
						<option value="3">填空题</option>
						<option value="4">简答题</option>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label class="checkbox-inline">
						<input type="checkbox" id="cb" name="cb" onchange="changestatement()" value="0">学生可见
					</label>
				</div>

			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-2 column">
				<label><h3>知识点</h3></label>
			</div>
			<div class="col-md-10 column">

				<div class="form-group">
					<br /> <select style="width: 300px" id="knowlege">
						<%
							for (Object obj : list) {
								OKnowledge ob = (OKnowledge) obj;
						%>
						<option value="<%=ob.getKnowledgepoint()%>" selected><%=ob.getKnowledgepoint()%></option>
						<%
							}
						%>
					</select>
				</div>


			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-2 column">
				<label><h3>题目</h3></label>
			</div>
			<div class="col-md-10 column">

				<div class="form-group">
					<br />
					<textarea name="content1" class="form-control" rows="5"></textarea>
					<!-- <input type="button" value="提 交" class="ajaxpost" id="submit" onclick="submitForm();"/> -->
				</div>

			</div>
		</div>
		<div class="panel-body">
			<div id="d" class="col-md-2 column" style="display:none ;">
				<label><h3>答案</h3></label>
			</div>
			<!--选择题-->
			<div id="d1" style="display: none;" class="col-md-10 column">

				<div class="form-group">
					<br />
					<div>
						共 <input type="text" name="input1" id="answer" value=""
							onchange="fun()" />个选项 正解答案为:
						<div id="choose"></div>
						<label style="color: red;">(请写入选择题选项个数并选择正确选项)</label>
					</div>

				</div>

			</div>
			<!--判断题-->
			<div id="d2" class="col-md-10 column" style="display: none;">

				<div class="form-group">
					<br /> <input type="text" name="" id="panduan" value="true" /> <label
						class="radio-inline" > <input type="radio"
						name="optionsRadiosInline" id="ori1"
						value="true" checked="" onclick="fun4()">true
					</label> <label class="radio-inline"> <input type="radio"
						name="optionsRadiosInline" id="ori2"
						value="false" onclick="fun4()">false
					</label> <label style="color: red;">(请直接填写正确答案)</label>
				</div>

			</div>
			<div id="d3" class="col-md-10 column" style="display: none;">

				<div class="form-group">
					<br /> <input type="text" name="" id="tiankong" value="" /> <label
						style="color: red;">(请直接填写正确答案)</label>
				</div>

			</div>
			<div  class="col-md-10 column"  >

				<div id="d4" class="form-group" >
					<br />
					<textarea id="content_3" name="content3" class="form-control"
						rows="3"></textarea>
					<label style="color: red;">(请直接填写正确答案)</label>
				</div>

			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-2 column">
				<label><h3>分值</h3></label>
			</div>
			<div class="col-md-10 column">

				<div class="form-group">
					<br /> <input type="text" name="" id="qvalue" value="" />
				</div>

			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-2 column">
				<label><h3>备注</h3></label>
			</div>
			<div class="col-md-10 column">

				<div class="form-group">
					<br />
					<textarea id="content_2" name="content2" class="form-control"
						rows="3"></textarea>
				</div>

			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-12 column">
				<center>
					<input class="btn btn-primary" type="button" name="" id=""
						value="插入" onclick="main()" />
				</center>
			</div>
		</div>
		<!-- </form>  -->
	</div>
	<!-- <div id="contentView"></div> -->
	<!-- <script type="text/javascript">
		$("#select").change(function() {
			var str = $("#select").find("option:selected").text();
			alert(str);
		})
		$("#knowlege").change(function() {
			var str = $("#knowlege").find("option:selected").text();
			alert(str);
		})
	</script> -->

	<!--题型选择事件触发-->
	<script type="text/javascript">
		$("#select").change(function() {
			var src = $("#select").val()
			src = parseInt(src);
			switch (src) {
			case 1://选择题
				$("#d").attr("style", "display:");
				$("#d1").attr("style", "display:");
				$("#d2").attr("style", "display:none");
				$("#d3").attr("style", "display:none");
				$("#d4").attr("style", "display:none");				
				break;
			case 2://判断题
				$("#d").attr("style", "display:");
				$("#d1").attr("style", "display:none");
				$("#d2").attr("style", "display:");
				$("#d3").attr("style", "display:none");
				$("#d4").attr("style", "display:none");
				break;
			case 3://填空题
				$("#d").attr("style", "display:");
				$("#d1").attr("style", "display:none");
				$("#d2").attr("style", "display:none");
				$("#d3").attr("style", "display:");
				$("#d4").attr("style", "display:none");
				break;
			case 4://简答题
				$("#d").attr("style", "display:");
				$("#d1").attr("style", "display:none");
				$("#d2").attr("style", "display:none");
				$("#d3").attr("style", "display:none");
				$("#d4").attr("style", "display:");
				break;
			default:
				break;
			}
		});
	</script>

	<script type="text/javascript">
	<!--当题型为选择题时，option选项事件触发-->
		function fun() {
			var n = $("#answer").val()
			var src = '';
			var html = '';
			for (var i = 0; i < n; i++) {
				src = String.fromCharCode((65 + i));
				html += '<label class=\"checkbox-inline\"><input id=' +src+ ' type=\"checkbox\"  value="'+src+'">'
						+ src + '</label>';				    
			}
			 $('#choose').html(html);			
		}
		/* 复选框“学生是否可见”事件触发 */
		function changestatement(){
			var check = document.getElementById("cb");
			
             if(check.checked == true){
                 document.getElementById("cb").value = "0";
            }else{
                 document.getElementById("cb").value = "1";
             } 
		}
		function fun3(){
			var val=$("#answer").val();
			var arr=new Array();
			for (var i = 0; i < val; i++) {
				cha = String.fromCharCode((65 + i));
				if($("#"+cha).is(":checked")){
					arr.push($("#"+cha).val());
				}				    
			}
			return arr;
		}
		function fun4(){
			if($("#ori1").is(":checked")){
			//	alert($("#ori1").val());
				document.getElementById("panduan").value=$("#ori1").val();
			}
			if($("#ori2").is(":checked")){
			//	alert($("#ori2").val());
				document.getElementById("panduan").value=$("#ori2").val();
			}
		}
		
		
	</script>
</body>
</html>
