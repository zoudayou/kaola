<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd 

">
<html>
<head>

<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300'
	rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<%=basePath%>/js/jquery-3.1.1.js"></script>
<link rel="stylesheet" href="<%=basePath%>/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=basePath%>/css/animate.css">
<link rel="stylesheet" href="<%=basePath%>/css/style.css">
<script type="text/javascript">
     // 更换验证码

function changeCode(){
		document.getElementById("captchaImage").src="captcha.form?tf=n&t="
			+ new Date().getTime();
}
</script>
</head>
<body>
	<br>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<!-- Start Sign In Form -->
			<form action="adminis.html" method="post" name="myform"
				class="fh5co-form animate-box" data-animate-effect="fadeIn">
				<h2>Sign In</h2>
				<div id="message">${error}<span id="showname"
						style="display: inline;"></span>
				</div>
				<div class="form-group">
					<label for="username" class="sr-only">Username</label> <input
						type="text" class="form-control" id="name" name="username"
						value="lh" onblur="show()" placeholder="Username" autocomplete="off">
				</div>
				<div class="form-group">
					<label for="password" class="sr-only">Password</label> <input
						type="password" class="form-control" id="pwd" name="password"
						value="lh123456" onblur="show()" placeholder="Password" autocomplete="off">
				</div>
				<div class="form-group">
				<tr>
    			<th>captcha</th>
    			<td>
             	<input type="text" id="captcha" name="captcha" class="text" maxlength="10" />
              	<img id="captchaImage" src="captcha.form" title="看不清？单击换一张图片" onclick="changeCode()"/>
      			</td>
				</tr>
				</div> 

				<div class="form-group">
					<p>
						Not registered? <a href="signup.jsp">Sign Up</a>
					</p>
				</div>
				<div class="form-group">
					<input type="button" value="Sign In" class="btn btn-primary"
						onclick="document.myform.submit();">
				</div>
				<div class="form-group">
					<p>
						<a href="test1.jsp">哈哈哈</a>
					</p>
				</div>
			</form>
			<!-- END Sign In Form -->

		</div>
	</div>
</body>
</html>
