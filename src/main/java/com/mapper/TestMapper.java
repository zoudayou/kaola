package com.mapper;
import java.util.ArrayList;
import java.util.Map;
import com.pojo.OTest;
public interface TestMapper {
	 
	    /**
	     * 添加用户信息
	     *
	     * @param param
	     */
	    public void insertTest(Map<String, Object> param);
	 
	    /**
	     * 删除用户信息
	     *
	     * @param id
	     */
	    public void deleteTest(int id);
	 
	    /**
	     * 修改用户信息
	     *
	     * @param param
	     */
	    public void updateTest(Map<String, Object> param);
	 
	    /**
	     * 查询用户信息(后台)
	     *
	     * @return ArrayList<Users>
	     */
	    public ArrayList<OTest> searchTest();
	 
	    /**
	     * 根据用户名称查询用户信息(后台)
	     *
	     * @return ArrayList<Users>
	     */
	    public ArrayList<OTest> searchUsersByTestname(String tname);
	 
	    /**
	     * 根据编号查询用户信息
	     *
	     * @param id
	     * @return Users
	     */
	    public OTest searchTestById(int tid);
	 
	    /**
	     * 根据用户名和密码查询用户是否存在
	     *
	     * @param param
	     * @return Users
	     */
	    public OTest getUserByUsernameAndPassword(Map<String, Object> param);
	    
	    public ArrayList<OTest> SelectTestinfo();
	 
	
}
