package com.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.pojo.OKnowledge;
import com.pojo.OQuestionbank;

public interface QuestionMapper {
	public void insertQuestion(Map<String, Object> param);
    public void updateQuestion(Map<String, Object> param);
    public void updateStatement1(Map<String, Object> param);
	public ArrayList<OQuestionbank> searchAllques(Map<String, Object> param);
	public ArrayList<OKnowledge> selectknowledge();
	
	
	/*public ArrayList<OKnowledge> searchknowledge();
	public ArrayList<OKnowledge> searchquestiontype();*/
	
	
//	public ArrayList<OQuestionbank> searchUsersBytestid(int testid);

}
