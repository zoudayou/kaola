package com.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.pojo.OTest;
import com.pojo.OUser;
import com.pojo.RStudentTest;



public interface R_Student_TestMapper {
    /**
     * 添加用户信息
     * 试卷编号testid，用户编号uid，状态statement
     * @param param
     */
    public void insertStu_Test(Map<String, Object> param);
    /**
     * 添加用户信息
     * 批阅人编号tid，批阅人reader
     * @param param
     */


    public void delStu_Test(HashMap<String, Object> param);
 
    /**
     * 修改用户信息
     *
     * @param param
     */
    public void updateStu_Test(Map<String, Object> param);
 
   /**
     * 查询用户信息(后台)
     *
     * @return ArrayList<Users>
     */
    public ArrayList<RStudentTest> searchStu_Test();
 
    /**
     * 根据用户名称查询用户信息(后台)
     *
     * @return ArrayList<Users>
     */
    /*public ArrayList<RStudentTest> searchStu_TestByUserid(int stuid);*/
 
 /**
     * 根据编号查询用户信息
     *
     * @param id
     * @return Users
     */
    public ArrayList<RStudentTest> searchUsersBytestid(int testid);
 
    /**
     * 根据用户名和密码查询用户是否存在
     *
     * @param param
     * @return Users
     */
}
