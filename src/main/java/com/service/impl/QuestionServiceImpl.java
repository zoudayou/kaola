package com.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mapper.QuestionMapper;
import com.pojo.OKnowledge;
import com.pojo.OQuestionbank;
import com.service.QuestionService;

@Service
@Transactional
// @Transactional表示该类被Spring作为管理事务的类
public class QuestionServiceImpl {
	@Resource
	private QuestionMapper questionMapper;

	
	public void insertQuestion(Map<String, Object> param) {
		// TODO Auto-generated method stub
		questionMapper.insertQuestion(param);
	}

	
	public void updateQuestion(Map<String, Object> param) {
		// TODO Auto-generated method stub
		questionMapper.updateQuestion(param);
	}

	
	public ArrayList<OQuestionbank> searchAllques(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return questionMapper.searchAllques(param);
	}

	
	public ArrayList<OKnowledge> selectknowledge() {
		// TODO Auto-generated method stub
		return questionMapper.selectknowledge();
	}

	public void updateStatement1(Map<String, Object> param) {
		questionMapper.updateStatement1(param);
	}
	



	

}
