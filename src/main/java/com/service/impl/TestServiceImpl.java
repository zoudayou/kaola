package com.service.impl;


import java.util.ArrayList;
import java.util.Map;
 
import javax.annotation.Resource;
 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pojo.OTest;
import com.pojo.OUser;
import com.service.TestService;
import com.service.UsersService;
import com.mapper.TestMapper;


@Service
@Transactional
public class TestServiceImpl implements TestService{

	// @Transactional表示该类被Spring作为管理事务的类
	    @Resource
	    private TestMapper testMapper;
	 
	    /**
	     * 添加用户信息
	     *
	     * @param param
	     */
	    @Override
	    public void insertTest(Map<String, Object> param) {
	    	testMapper.insertTest(param);
	    }
	 
	    /**
	     * 删除用户信息
	     *
	     * @param id
	     */
	    @Override
	    public void deleteTest(int id) {
	    	testMapper.deleteTest(id);
	    }
	 
	    /**
	     * 修改用户信息
	     *
	     * @param param
	     */
	    @Override
	    public void updateTest(Map<String, Object> param) {
	    	testMapper.updateTest(param);
	    }
	 
	    /**
	     * 查询用户信息(后台)
	     *
	     * @return List<Users>
	     */
	    @Override
	    public ArrayList<OTest> searchTest() {
	        return testMapper.searchTest();
	    }
	 
	    /**
	     * 根据用户名称查询用户信息(后台)
	     *
	     * @return List<Users>
	     */
	    @Override
	    public ArrayList<OTest> searchUsersByTestname(String testname) {
	        return testMapper.searchUsersByTestname(testname);
	    }
	 
	    /**
	     * 根据编号查询用户信息
	     *
	     * @param id
	     * @return Users
	     */
	    @Override
	    public OTest searchTestById(int id) {
	        return testMapper.searchTestById(id);
	    }


	 
	    /**
	     * 根据用户名和密码查询用户是否存在
	     *
	     * @param param
	     * @return Users
	     */
		@Override
		public OTest getUserByUsernameAndPassword(Map<String, Object> param) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ArrayList<OTest> SelectTestinfo() {
			// TODO Auto-generated method stub
			return testMapper.SelectTestinfo();
		}
	   
}
