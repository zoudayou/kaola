package com.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cookie.CookieConstantTable;
import com.cookie.CookieUtils;
import com.cookie.EncryptionUtil;
import com.mapper.UsersMapper;
import com.pojo.OUser;
import com.service.UsersService;
 
@Service
@Transactional
// @Transactional表示该类被Spring作为管理事务的类
public class UsersServiceImpl implements UsersService {
    @Resource
    private UsersMapper usersMapper;
 
    /**
     * 添加用户信息
     *
     * @param param
     */
    @Override
    public void insertUsers(Map<String, Object> param) {
        usersMapper.insertUsers(param);
    }
 
    /**
     * 删除用户信息
     *
     * @param id
     */
    @Override
    public void deleteUsers(int id) {
        usersMapper.deleteUsers(id);
    }
 
    /**
     * 修改用户信息
     *
     * @param param
     */
    @Override
    public void updateUsers(Map<String, Object> param) {
        usersMapper.updateUsers(param);
    }
 
    /**
     * 查询用户信息(后台)
     *
     * @return List<Users>
     */
    @Override
    public ArrayList<OUser> searchUsers() {
        return usersMapper.searchUsers();
    }
 
    /**
     * 根据用户名称查询用户信息(后台)
     *
     * @return List<Users>
     */
    @Override
    public ArrayList<OUser> searchUsersByUsername(String username) {
        return usersMapper.searchUsersByUsername(username);
    }
 
    /**
     * 根据编号查询用户信息
     *
     * @param id
     * @return Users
     */
    @Override
    public OUser searchUsersById(int id) {
        return usersMapper.searchUsersById(id);
    }
 
    /**
     * 根据用户名和密码查询用户是否存在
     *
     * @param param
     * @return Users
     */
    public OUser getUserByUsernameAndPassword(Map<String, Object> param) {
        return usersMapper.getUserByUsernameAndPassword(param);
    }

	@Override
	public String searchUrightByUsername(String username) {
		// TODO Auto-generated method stub
		return usersMapper.searchUrightByUsername(username);
	}
    
}


