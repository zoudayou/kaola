package com.service.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mapper.R_Student_TestMapper;
import com.pojo.OTest;
import com.pojo.OUser;
import com.pojo.RStudentTest;



@Service
@Transactional
public class R_Student_TestServiceImpl {



// @Transactional表示该类被Spring作为管理事务的类

    @Resource
    private  R_Student_TestMapper rstMapper;

    
    /**
     * 添加用户信息
     *
     * @param param
     */

public void insertStu_Test(HashMap<String, Object> param) {
	// TODO Auto-generated method stub
	rstMapper.insertStu_Test(param);
}

    /**
     * 删除用户信息
     *
     * @param id
     */

	public void delStu_Test(HashMap<String, Object> param) {
		// TODO Auto-generated method stub
		rstMapper.delStu_Test(param);
	}

    /**
     * 修改用户信息
     *
     * @param param
     */
	public void updateStu_Test(Map<String, Object> param) {
		// TODO Auto-generated method stub
		rstMapper.updateStu_Test(param);
		
	}
/*    *//**
     * 查询用户信息(后台)
     *
     * @return List<Users>
     */

	public ArrayList<RStudentTest> searchStu_Test() {
		// TODO Auto-generated method stub
		return rstMapper.searchStu_Test();
	}
/*
	@Override
	public ArrayList<OUser> searchStu_TestByUserid(int stuid) {
		// TODO Auto-generated method stub
		return rstMapper.searchStu_TestByUserid(stuid);
	}*/

	
	public ArrayList<RStudentTest> searchUsersBytestid(int testid) {
		// TODO Auto-generated method stub
		return rstMapper.searchUsersBytestid(testid);
	}

}