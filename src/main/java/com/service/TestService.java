package com.service;

import java.util.ArrayList;
import java.util.Map;
import com.pojo.OTest;
public interface TestService {
	     
	    public void insertTest(Map<String, Object> param);
	     
	    
	    /**
	     * 添加用户信息
	     * @param param
	     */
	    
	    
	    
	    public void deleteTest(int id);
	    /**
	     * 删除用户信息
	     * @param id
	     */
	    
	    public void updateTest(Map<String, Object> param);
	     
	    /**
	     * 修改用户信息
	     * @param param
	     */
	    public ArrayList<OTest> searchTest();
	    /**
	     * 查询用户信息(后台)
	     * @return List<Users>
	     */
	    
	    public ArrayList<OTest> searchUsersByTestname(String uname);
	    /**
	     * 根据用户名称查询用户信息(后台)
	     * @return List<Users>
	     */
	    
	    public OTest searchTestById(int id);
	    /**
	     * 根据编号查询用户信息
	     * @param id
	     * @return Users
	     */
	    
	    public OTest getUserByUsernameAndPassword(Map<String, Object> param);

	    /**
	     * 根据用户名和密码查询用户是否存在
	     * @param param
	     * @return Users
	     */
	    
	    public ArrayList<OTest> SelectTestinfo();

	

}
