package com.service;

import java.util.ArrayList;
import java.util.Map;
import com.pojo.OUser;

public interface UsersService {
     
    public void insertUsers(Map<String, Object> param);
     
    
    /**
     * 添加用户信息
     * @param param
     */
    
    
    
    public void deleteUsers(int id);
    /**
     * 删除用户信息
     * @param id
     */
    
    public void updateUsers(Map<String, Object> param);
     
    /**
     * 修改用户信息
     * @param param
     */
    public ArrayList<OUser> searchUsers();
    /**
     * 查询用户信息(后台)
     * @return List<Users>
     */
    
    public ArrayList<OUser> searchUsersByUsername(String uname);
    /**
     * 根据用户名称查询用户信息(后台)
     * @return List<Users>
     */
    
    public OUser searchUsersById(int id);
    /**
     * 根据编号查询用户信息
     * @param id
     * @return Users
     */
    
    public OUser getUserByUsernameAndPassword(Map<String, Object> param);

    /**
     * 根据用户名和密码查询用户是否存在
     * @param param
     * @return Users
     */
    public String searchUrightByUsername(String username);

}
