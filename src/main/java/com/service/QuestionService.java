package com.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.pojo.OKnowledge;
import com.pojo.OQuestionbank;

public interface QuestionService {
	public void insertQuestion(Map<String, Object> param);
    public void updateQuestion(Map<String, Object> param);
	public ArrayList<OQuestionbank> searchAllques(Map<String, Object> param);
	public ArrayList<OKnowledge> selectknowledge();

}
