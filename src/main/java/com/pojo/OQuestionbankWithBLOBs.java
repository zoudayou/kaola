package com.pojo;

public class OQuestionbankWithBLOBs extends OQuestionbank {
    private String question;

    private String modanswer;

    private String assess;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    public String getModanswer() {
        return modanswer;
    }

    public void setModanswer(String modanswer) {
        this.modanswer = modanswer == null ? null : modanswer.trim();
    }

    public String getAssess() {
        return assess;
    }

    public void setAssess(String assess) {
        this.assess = assess == null ? null : assess.trim();
    }
}