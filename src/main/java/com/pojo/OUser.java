package com.pojo;

import java.util.Date;

public class OUser {
    private Long uid;

    private String uname;

    private String upwd;

    private String utele;

    private String uemail;

    private String ubase;

    private String utruename;

    private String usex;

    private Date ubirthdate;

    private String ulocation;

    private Long uqq;

    private String uhobbies;

    private String uimage;

    private String uright;
    
    private String captcha;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname == null ? null : uname.trim();
    }

    public String getUpwd() {
        return upwd;
    }

    public void setUpwd(String upwd) {
        this.upwd = upwd == null ? null : upwd.trim();
    }

    public String getUtele() {
        return utele;
    }

    public void setUtele(String utele) {
        this.utele = utele == null ? null : utele.trim();
    }

    public String getUemail() {
        return uemail;
    }

    public void setUemail(String uemail) {
        this.uemail = uemail == null ? null : uemail.trim();
    }

    public String getUbase() {
        return ubase;
    }

    public void setUbase(String ubase) {
        this.ubase = ubase == null ? null : ubase.trim();
    }

    public String getUtruename() {
        return utruename;
    }

    public void setUtruename(String utruename) {
        this.utruename = utruename == null ? null : utruename.trim();
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex == null ? null : usex.trim();
    }

    public Date getUbirthdate() {
        return ubirthdate;
    }

    public void setUbirthdate(Date ubirthdate) {
        this.ubirthdate = ubirthdate;
    }

    public String getUlocation() {
        return ulocation;
    }

    public void setUlocation(String ulocation) {
        this.ulocation = ulocation == null ? null : ulocation.trim();
    }

    public Long getUqq() {
        return uqq;
    }

    public void setUqq(Long uqq) {
        this.uqq = uqq;
    }

    public String getUhobbies() {
        return uhobbies;
    }

    public void setUhobbies(String uhobbies) {
        this.uhobbies = uhobbies == null ? null : uhobbies.trim();
    }

    public String getUimage() {
        return uimage;
    }

    public void setUimage(String uimage) {
        this.uimage = uimage == null ? null : uimage.trim();
    }

    public String getUright() {
        return uright;
    }

    public void setUright(String uright) {
        this.uright = uright == null ? null : uright.trim();
    }
    
    public String getcaptcha() { 
        return captcha; 
    } 
     
    public void setcaptcha(String captcha) { 
        this.captcha = captcha; 
    }
}