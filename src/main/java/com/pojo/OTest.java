package com.pojo;

import java.util.Date;

public class OTest {
    private Long testid;

    private String testname;

    private String testsub;

    private Date testtime;

    private Date endtime;

    private String maker;

    private Integer cid;

    private String cname;

    private String indstudent;

    private Integer attendnum;

    private Integer allnum;

    private Integer waitcheck;

    private Integer checkover;

    private Integer statement;

    public Long getTestid() {
        return testid;
    }

    public void setTestid(Long testid) {
        this.testid = testid;
    }

    public String getTestname() {
        return testname;
    }

    public void setTestname(String testname) {
        this.testname = testname == null ? null : testname.trim();
    }

    public String getTestsub() {
        return testsub;
    }

    public void setTestsub(String testsub) {
        this.testsub = testsub == null ? null : testsub.trim();
    }

    public Date getTesttime() {
        return testtime;
    }

    public void setTesttime(Date testtime) {
        this.testtime = testtime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker == null ? null : maker.trim();
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public String getIndstudent() {
        return indstudent;
    }

    public void setIndstudent(String indstudent) {
        this.indstudent = indstudent == null ? null : indstudent.trim();
    }

    public Integer getAttendnum() {
        return attendnum;
    }

    public void setAttendnum(Integer attendnum) {
        this.attendnum = attendnum;
    }

    public Integer getAllnum() {
        return allnum;
    }

    public void setAllnum(Integer allnum) {
        this.allnum = allnum;
    }

    public Integer getWaitcheck() {
        return waitcheck;
    }

    public void setWaitcheck(Integer waitcheck) {
        this.waitcheck = waitcheck;
    }

    public Integer getCheckover() {
        return checkover;
    }

    public void setCheckover(Integer checkover) {
        this.checkover = checkover;
    }

    public Integer getStatement() {
        return statement;
    }

    public void setStatement(Integer statement) {
        this.statement = statement;
    }
}