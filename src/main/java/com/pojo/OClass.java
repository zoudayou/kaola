package com.pojo;

import java.util.Date;

public class OClass {
    private Long cid;

    private String cname;

    private Date coptime;

    private Date cgratime;

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public Date getCoptime() {
        return coptime;
    }

    public void setCoptime(Date coptime) {
        this.coptime = coptime;
    }

    public Date getCgratime() {
        return cgratime;
    }

    public void setCgratime(Date cgratime) {
        this.cgratime = cgratime;
    }
}