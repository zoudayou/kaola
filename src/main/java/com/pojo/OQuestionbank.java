package com.pojo;

public class OQuestionbank {
    private Long qid;

    private String knowledgepoint;

    private String questiontype;

    private String qoption;

    private Integer qvalue;

    private Integer statement1;

    private Integer statement2;
    
    private String question;

    private String modanswer;

    private String assess;

    public Long getQid() {
        return qid;
    }

    public void setQid(Long qid) {
        this.qid = qid;
    }

    public String getKnowledgepoint() {
        return knowledgepoint;
    }

    public void setKnowledgepoint(String knowledgepoint) {
        this.knowledgepoint = knowledgepoint == null ? null : knowledgepoint.trim();
    }

    public String getQuestiontype() {
        return questiontype;
    }

    public void setQuestiontype(String questiontype) {
        this.questiontype = questiontype == null ? null : questiontype.trim();
    }

    public String getQoption() {
        return qoption;
    }

    public void setQoption(String qoption) {
        this.qoption = qoption == null ? null : qoption.trim();
    }

    public Integer getQvalue() {
        return qvalue;
    }

    public void setQvalue(Integer qvalue) {
        this.qvalue = qvalue;
    }

    public Integer getStatement1() {
        return statement1;
    }

    public void setStatement1(Integer statement1) {
        this.statement1 = statement1;
    }

    public Integer getStatement2() {
        return statement2;
    }

    public void setStatement2(Integer statement2) {
        this.statement2 = statement2;
    }
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    public String getModanswer() {
        return modanswer;
    }

    public void setModanswer(String modanswer) {
        this.modanswer = modanswer == null ? null : modanswer.trim();
    }

    public String getAssess() {
        return assess;
    }

    public void setAssess(String assess) {
        this.assess = assess == null ? null : assess.trim();
    }
}