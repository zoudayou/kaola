package com.controller;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.service.impl.R_Student_TestServiceImpl;



@Controller
@RequestMapping("/")
public class R_Student_TestContorller {
	protected final Log logger = LogFactory.getLog(getClass());
    
	@Resource
    private R_Student_TestServiceImpl rstService;
	
	 @RequestMapping("insertR_Student_Test")
	    public String adminis(@Param("testid") Integer testid,@Param("uid") Integer uid,HttpServletRequest request,Model model) {
	        HttpSession session = request.getSession();
	        session.setAttribute("testid",testid);
	        session.setAttribute("uid",uid);       
	        HashMap<String, Object> users = new HashMap<String, Object>();
	        users.put("testid",testid);
	        users.put("uid",uid);
	        System.out.print(users);
	        rstService.insertStu_Test(users);
	        return "succuss1";	        
	    }
	 
	 @RequestMapping("delStu_Test")
	    public String delStu_Test(@Param("testid") Integer testid,@Param("uid") Integer uid,HttpServletRequest request,Model model) {
	        HttpSession session = request.getSession();
	        session.setAttribute("testid",testid);
	        session.setAttribute("uid",uid);       
	        HashMap<String, Object> users = new HashMap<String, Object>();
	        users.put("testid",testid);
	        users.put("uid",uid);
	        System.out.print(users);
	        rstService.delStu_Test(users);
	        return "succuss1";	        
	    }
	 
	 @RequestMapping("updateStu_Test")
	    public String updateStu_Test(@Param("testid") Integer testid,@Param("grade") Integer grade,
	    		@Param("tid") Integer tid,@Param("reader") String reader,@Param("statement") Integer statement,
	    		@Param("uid") Integer uid,HttpServletRequest request,Model model) {
	        HttpSession session = request.getSession();
	        session.setAttribute("testid",testid);
	        session.setAttribute("uid",uid);
	        session.setAttribute("tid",tid);
	        session.setAttribute("reader",reader);
	        session.setAttribute("grade",grade);
	        session.setAttribute("statement",statement);
	        HashMap<String, Object> users = new HashMap<String, Object>();
	        users.put("testid",testid);
	        users.put("uid",uid);
	        users.put("tid",tid);
	        users.put("reader",reader);
	        users.put("grade",grade);
	        users.put("statement",statement);
	        System.out.print(users);
	        rstService.updateStu_Test(users);
	        return "succuss1";	        
	    }
	 @RequestMapping("SelectAll")
	 	@ResponseBody
	    public String SelectAll(HttpServletRequest request,Model model) throws FileNotFoundException {    
		 	List list = rstService.searchStu_Test();
	        String s = JSON.toJSONString(list);
	        System.out.print(s);
			request.setAttribute("list", s);
	        return s;	        
	    }
	 @RequestMapping("SelectByTestid")
	 	@ResponseBody
	    public String SelectByTestid(@Param("testid") Integer testid,HttpServletRequest request,Model model) throws FileNotFoundException {
		 	HttpSession session = request.getSession();
	        session.setAttribute("testid",testid);	        
		 	List list = rstService.searchUsersBytestid(testid);
	        String s = JSON.toJSONString(list);
	        System.out.print(s);
			request.setAttribute("list", s);
	        return s;	        
	    }

}
