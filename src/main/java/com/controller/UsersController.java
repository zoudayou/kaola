package com.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.pojo.OUser;
import com.service.TestService;
import com.login.CaptchaUtil.CaptchaUtil;
import com.mapper.UsersMapper;
import com.service.UsersService;
 
@Controller
@RequestMapping("/")
public class UsersController {
 
    protected final Log logger = LogFactory.getLog(getClass());
 
    @Resource
    private UsersService usersService;
     
    @RequestMapping("main")
    public String main() {
        return "main";
    }
 
    @RequestMapping("login")
    public String login() {
         
            return "login";
         
        /*HashMap<String, Object> users = new HashMap<String, Object>();
        users.put("username",username.trim());
        users.put("password",MD5Util.string2MD5(password.trim()));
        Users one=usersService.getUserByUsernameAndPassword(users);
        HttpSession session = request.getSession();
        session.setAttribute("one",one);
        if(one==null){
            model.addAttribute("error","用户名或密码错误！");
            return "login";
        }else{
            return "redirect:/main";
        }*/
    }
     
    @RequestMapping("adminis")
    public String adminis(@Param("username") String username,@Param("password") String password,@Param("captcha") String captcha,HttpServletRequest request,Model model) {
        HttpSession session = request.getSession();
        session.setAttribute("username",username);
        session.setAttribute("password",password);
     
//        System.out.println(username);
        //session.setAttribute("captcha", captcha);
        if(username==null||username==""){
            model.addAttribute("error","用户名不能为空！");
            return "login";
        }else if(password==null||password==""){
            model.addAttribute("error","密码不能为空！");
            return "login";
        }
        else if(captcha==null||captcha=="") {
        	model.addAttribute("error","验证码不能为空！");
        	return "login";
        }
        
        HashMap<String, Object> users = new HashMap<String, Object>();
        users.put("username",username.trim());
        users.put("password",password.trim());
//        users.put("captcha", captcha.trim());
        OUser user=usersService.getUserByUsernameAndPassword(users);
//        String code = (String) request.getSession().getAttribute("captcha");// 得到session中的正确验证码

        String randomString=(String)session.getAttribute("randomString");
        session.setAttribute("user",user);
        String right=usersService.searchUrightByUsername(username);
        System.out.println(right);
        if(user==null){
            model.addAttribute("error","用户名或密码错误！");
            return "login";
        }
        else if (!randomString.equals(captcha.toUpperCase())) {
        	model.addAttribute("error","验证码错误！");
            return "login"; 	
        }
        else{
        	if(right.equals("学生")||right.equals("stu"))
        		return "stu/stument-main";
        	else if(right.equals("老师"))
        		return "teacher/teacment-main";
        	else
        		return "admin/adment-main";
        //	return "stu/stument-main";
         //   return "redirect:/admin";
        }
		
        
    }
     
  
     
    @RequestMapping("logout")
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        request.getSession().invalidate();
        return "login";
    }
    /**
     * 添加用户
     * @param user
     * @return
     */
    private UsersService ueService;

    @RequestMapping("insertUser")
    public String insertUser(@Param("username") String name,@Param("password") String psw,@Param("utele") String utele,@Param("uemail") String uemail,HttpServletRequest request,Model model) {
        HttpSession session = request.getSession();
        session.setAttribute("username",name);
        session.setAttribute("password",psw);
        session.setAttribute("utele", utele);
        session.setAttribute("uemail", uemail);
        if(name==null||name==""){
            model.addAttribute("error","用户名不能为空！");
            return "signup";
        }else if(psw==null||psw==""){
            model.addAttribute("error","密码不能为空！");
            return "signup";
        }
        else if(utele==null||utele==""){
            model.addAttribute("error","utele不能为空！");
            return "signup";
        }
        else if(uemail==null||uemail==""){
            model.addAttribute("error","uemail不能为空！");
            return "signup";
        }
        HashMap<String, Object> users = new HashMap<String, Object>();
        users.put("username",name.trim());
        users.put("password",psw.trim());
        users.put("utele", utele.trim());
        users.put("uemail", uemail.trim());
        usersService.insertUsers(users);
        return "succuss";
    }    
    
    @RequestMapping(value = "captcha", method = RequestMethod.GET)
    @ResponseBody
    public void captcha(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        CaptchaUtil.outputCaptcha(request, response);
        
    }
    
   
 

}
