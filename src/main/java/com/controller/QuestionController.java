package com.controller;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mapper.QuestionMapper;
import com.pojo.OQuestionbank;
import com.service.QuestionService;
import com.service.impl.QuestionServiceImpl;

@Controller
@RequestMapping("/")
public class QuestionController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Resource
	private QuestionMapper questionService;
	
	@RequestMapping(value = "selectknowledge")
	public String selectknowledge(HttpServletRequest request, Model model) throws FileNotFoundException {
		HttpSession session = request.getSession();
		List list = questionService.selectknowledge();
		request.setAttribute("list", list);
		return "teacher/question_add";
	}

	@RequestMapping("insertQuestion")
	@ResponseBody	
	public String insertQuestion(HttpServletRequest request,  OQuestionbank oqb) {
		HttpSession session=request.getSession();		
		HashMap<String, Object> tests = new HashMap<String, Object>();
		tests.put("questiontype", oqb.getQuestiontype());
		tests.put("knowledgepoint", oqb.getKnowledgepoint());
		tests.put("statement2", oqb.getStatement2());
		tests.put("qoption", oqb.getQoption());
		tests.put("question", oqb.getQuestion());
		tests.put("modanswer", oqb.getModanswer());
		tests.put("qvalue", oqb.getQvalue());
		tests.put("assess", oqb.getAssess());
		questionService.insertQuestion(tests);
		System.out.println("oqb.getQoption()="+oqb.getQoption());
		/*System.out.println("oqb.getKnowledgepoint()="+oqb.getKnowledgepoint());*/
		return "teacher/question_add";
	}
	@RequestMapping("searchAllques")
	public String searchAllques(HttpServletRequest request, Model model) throws FileNotFoundException {	
		HttpSession session = request.getSession();
		System.out.println("222");
		List list1 = questionService.selectknowledge();
	//	System.out.println(list1);
		request.setAttribute("list1", list1);
		//从前台获取数据
		String knowledgepoint=request.getParameter("knowledgepoint");
		String questiontype=request.getParameter("questiontype");
		String question=request.getParameter("question");
		
		if(knowledgepoint.equals("请选择")) {
			knowledgepoint="";
		}
		if(questiontype.equals("请选择")) {
			questiontype="";
		}
		request.setAttribute("knowledgepoint", knowledgepoint);
		request.setAttribute("questiontype", questiontype);
		request.setAttribute("question", question);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("knowledgepoint", knowledgepoint);
		params.put("questiontype", questiontype);
		params.put("question", question);
		
		/*System.out.println("1"+params.get("knowledgepoint"));
		System.out.println("2"+params.get("questiontype"));
		System.out.println("3"+params.get("question"));*/
		List list2 =questionService.searchAllques(params);	
	//	System.out.println(list2);
		request.setAttribute("list2", list2);
		
		return "teacher/question_search";
	}
	
	@RequestMapping("updateStatement1")
	@ResponseBody	
	public String updateStatement1(HttpServletRequest request,  OQuestionbank oqb) {
		HttpSession session=request.getSession();		
		System.out.println("enter insertStatement1");
		HashMap<String, Object> change = new HashMap<String, Object>();
		change.put("statement1", oqb.getStatement1());
		System.out.println("oqb.getStatement1()="+oqb.getStatement1());
		System.out.println("oqb.getQid()"+oqb.getQid());
	
		questionService.updateStatement1(change);
	
		return "teacher/question_search";
	}
	
	
	/*public String insertQuestion(@Param("select") String questiontype, @Param("statement2") String statement2,
			@Param("knowledge") String knowledgepoint, @Param("content_1") String question,
			@Param("answer") String modanswer, @Param("qvalue") Integer qvalue, @Param("assess") String assess,
			HttpServletRequest request, Model model) {
		System.out.println("1111111");
		HttpSession session = request.getSession();
		
		HashMap<String, Object> tests = new HashMap<String, Object>();

		tests.put("questiontype", questiontype.trim());
		tests.put("knowledgepoint", knowledgepoint.trim());
		tests.put("statement2", statement2);
		tests.put("question", question.trim());
		tests.put("modanswer", modanswer.trim());
		tests.put("qvalue", qvalue);
		tests.put("assess", assess.trim());

		// System.out.println(tests.get("testname"));
		questionService.insertQuestion(tests);
		return "teacher/question-add";
	}*/

	/*@RequestMapping(value = "searchAllques", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String searchAllques(HttpServletRequest request, Model model) throws FileNotFoundException {
		List list = questionService.searchAllques();
		String s = JSON.toJSONString(list);
		System.out.print(s);
		request.setAttribute("list", s);
		return s;
	}*/

	

}
