package com.controller;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.pojo.OUser;
import com.service.TestService;
import com.alibaba.fastjson.JSON;
import com.login.CaptchaUtil.CaptchaUtil;
import com.mapper.UsersMapper;
import com.service.UsersService;
 
@Controller
@RequestMapping("/")
public class TestController {
	    @Resource//通过byname来自动注入
	    private TestService testService;

	    @RequestMapping("insertTest")
	    public String insertTest(@Param("testname") String testname,@Param("subject") String subject,HttpServletRequest request,Model model) {
	        HttpSession session = request.getSession();
	        session.setAttribute("testname",testname);
	        session.setAttribute("subject", subject);
	        //System.out.println(session.getAttribute("testname"));
	        if(testname==null||testname==""){
	            model.addAttribute("error","名字不能为空！");
	            return "test";
	        }
	        HashMap<String, Object> tests = new HashMap<String, Object>();
	        tests.put("testname",testname.trim());
	        tests.put("subject", subject.trim());
	        //System.out.println(tests.get("testname"));
	        testService.insertTest(tests);
	        return "succuss1";
	    }  
	    
	    @RequestMapping("searchUsersById")
	    public String selectexam(HttpServletRequest request,Model model) {
	        
	        return "stu/testload";
	    }

	    @RequestMapping(value="SelectTestinfo",produces = "application/json; charset=utf-8")
	 	@ResponseBody
	    public String SelectTestinfo(HttpServletRequest request,Model model) throws FileNotFoundException {    
		 	List list = testService.SelectTestinfo();
	        String s = JSON.toJSONString(list);
	        System.out.print(s);
			request.setAttribute("list", s);
	        return s;	        
	    }
}
